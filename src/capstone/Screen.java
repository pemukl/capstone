package capstone;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.input.Key.Kind;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.Terminal.Color;

public abstract class Screen {
	private static Screen shown;

	protected Screen parent;

	private static String message; // Im Moment oben links angezeigte Nachricht.
	private static boolean newMesssage;
	private static final int messageTime = 500; // so lang werden Nachrichten
												// angezeigt, falls sie endlich
												// sind.
	private static int mTime; // Zeit, die die aktuelle Nachricht noch hat,
								// bevor sie geloescht wird.
	private static Terminal.Color msgColor; // Farbe der Nachricht.

	public Screen(Screen parent) {
		this.parent = parent;
		msgColor = Terminal.Color.BLACK;
	}

	public Screen getNextScreen(Key input) {
		if (!(shown == this)) {
			show();
			shown = this;
		}

		if ((input != null) && (input.getKind() == Kind.Escape)
				&& (parent != null))
			return parent;

		updateMessage();

		Screen nextScreen = update(input);

		return nextScreen;

	}

	/**
	 * Diese Methode soll auf einen Input reagieren
	 * @param input
	 * @return den naechsten Screen.
	 */
	public abstract Screen update(Key input);

	/**
	 * Diese Methode soll den betreffenden Screen komplett neu laden
	 */
	public abstract void show();

	/**
	 * Diese Methode soll sich um eine Veraenderung der Bildschirmgroeße kuemmern.
	 */
	public abstract void actOnResize();

	/**
	 * Hiermit werden unten links waehrend des Spiels Nachrichten ausgegeben.
	 * 
	 * @param msg
	 *            anzuzeigende Nachricht
	 * @param infinite
	 *            gibt an, ob die Nachricht ewig bleiben soll.
	 * @param clr
	 *            gibt die Farbe der Nachricht an
	 */
	public static void print(String msg, boolean infinite, Terminal.Color clr) {

		newMesssage = true;
		message = msg;
		msgColor = clr;
		mTime = messageTime;
		if (infinite)
			mTime = -1;
	}

	/**
	 * Zeigt Nachtrichten auf dem DisplayManager.
	 * 
	 * @param pMessage
	 *            Nachricht, die angezeigt werden soll
	 * @param clr
	 *            Farbe
	 */
	public static void print(String message, Color clr) {
		print(message, false, clr);
	}

	/**
	 * Zeigt Nachtrichten auf dem DisplayManager.
	 * 
	 * @param pMessage
	 *            Nachricht, die angezeigt werden soll
	 * @param infinite
	 *            obd die Nachricht ewig bleibt
	 */
	public static void print(String message, boolean infinite) {
		print(message, infinite, Color.WHITE);
	}

	/**
	 * Zeigt Nachtrichten auf dem DisplayManager.
	 * 
	 * @param pMessage
	 *            Nachricht, die angezeigt werden soll
	 */
	public static void print(String message) {
		print(message, false, Color.WHITE);
	}

	/**
	 * Kuemmert sich um die unten links angezeigten Nachrichten: loescht wenn zu
	 * alt und zeigt sie sonst an.
	 */
	private void updateMessage() {
		if (newMesssage) {
			clearMessage();
			Main.term.applyForegroundColor(msgColor);
			Main.term.moveCursor(0, Main.term.getTerminalSize().getRows());
			Main.term.applyBackgroundColor(Color.BLACK);
			try {
				if (message != null)
					for (char c : message.toCharArray())
						Main.term.putCharacter(c);
			} catch (IndexOutOfBoundsException e) {
			}
			newMesssage = false;
		}

		if (mTime > 0)
			mTime--;

		if ((message != null) && (mTime <= 0) && (mTime != -1)) {
			message = null;
			clearMessage();
		}

	}

	/**
	 * Ueberschreibt die unterste Reihe mit schwarzen Feldern, um Nachricht zu
	 * entfernen.
	 */
	private static void clearMessage() {
		Main.term.moveCursor(0, Main.term.getTerminalSize().getRows());
		Main.term.applyBackgroundColor(Terminal.Color.BLACK);
		try {
			for (int i = 1; i <= Main.term.getTerminalSize().getColumns(); i++)
				Main.term.putCharacter(' ');
		} catch (IndexOutOfBoundsException e) {
		}

	}
}
