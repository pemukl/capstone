package capstone.menu.options;

import capstone.Main;
import capstone.Screen;
import capstone.game.boardMembers.Symbol;

import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc
 * Macht keine Aktion bei Auswahl sondert zeigt nur seine Texte an.
 * Dafuer evtl mit Symbol.
 */
public class TextOption extends Option {

	Symbol symbol;

	public TextOption(Symbol symbol, String name, String text) {
		super(name, text);
		this.symbol = symbol;
	}

	public TextOption(String name, String text) {
		this(null, name, text);
	}

	public TextOption(String name) {
		this(name, "");
	}

	@Override
	public void print(int row, boolean isSelected) {

		Main.term.moveCursor(0, row);

		if (symbol != null)
			try {
				Main.term.applyForegroundColor(symbol.clr.red,
						symbol.clr.green, symbol.clr.blue);
				Main.term.applyBackgroundColor(symbol.bgrd.red,
						symbol.bgrd.green, symbol.bgrd.blue);
				Main.term.putCharacter(symbol.chr);
				Main.term.applyBackgroundColor(Terminal.Color.BLACK);
				Main.term.putCharacter(' ');
			} catch (IndexOutOfBoundsException e) {
			}

		if (isSelected) {
			Main.term.applyBackgroundColor(Terminal.Color.WHITE);
			Main.term.applyForegroundColor(Terminal.Color.BLACK);
		} else {
			Main.term.applyBackgroundColor(Terminal.Color.BLACK);
			Main.term.applyForegroundColor(Terminal.Color.WHITE);
		}
		try {
			for (char c : toString().toCharArray())
				Main.term.putCharacter(c);
		} catch (IndexOutOfBoundsException e) {
		}

		if (isSelected)
			Screen.print(getDescription(), true);

	}

	@Override
	public Screen doAction() {
		return null;
	}

}
