package capstone.menu.options;

import capstone.Screen;

public class NextSceenOption extends Option{
	
	private Screen pointedScreen;

	
	public NextSceenOption(Screen pointedScreen, String name, String description) {
		super(name, description);
		this.pointedScreen = pointedScreen;
	}
	
	public NextSceenOption(Screen pointedScreen, String name) {
		this(pointedScreen, name, "");
	}

	@Override
	public Screen doAction() {
		return pointedScreen;
	}
	


}
