package capstone.menu.options;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import capstone.Main;
import capstone.Screen;
import capstone.game.Game;

/**
 * @author marc
 *Speichet ein Spiel bei Auswahl
 */
public class LevelSaveOption extends Option {
	File saveFile;

	public LevelSaveOption(File saveFile, String name, String description) {
		super(name, description);
		this.saveFile = saveFile;
	}

	public LevelSaveOption(File saveFile, String name) {
		this(saveFile, name, "");
	}

	@Override
	public Screen doAction() {

		File tmpFile = saveFile;
		if (tmpFile == null) {
			JFileChooser fileChooser = new JFileChooser();
			int userSelection = fileChooser.showSaveDialog(null);
			if (userSelection == JFileChooser.APPROVE_OPTION)
				tmpFile = fileChooser.getSelectedFile();
		}
		if (tmpFile != null)
			save(tmpFile);
		return null;
	}

	private void save(File file) {
		if (Main.getMainMenu().getGame().state == Game.GameState.NOTINITIALIZED) {
			System.err.println("Sorry, there is no Game initialized.");
			return;
		}

		Properties saves = new Properties();

		try {
			saves.load(new FileInputStream("saves.properties"));
		} catch (IOException ioe) {
			System.out.println("IO Error");
			System.out.println("Generateing new saves.properties.");
		}

		int numberOfSaves;
		if (saves.getProperty("numberOfSaves") == null)
			numberOfSaves = 0;
		else
			numberOfSaves = Integer
					.parseInt(saves.getProperty("numberOfSaves"));

		boolean isNew = true;

		for (int i = 1; i <= numberOfSaves; i++)
			if (file.getAbsolutePath().equals(
					saves.getProperty(Integer.toString(i))))
				isNew = false;

		if (Main.getMainMenu().getGame().save(file) && isNew) {
			numberOfSaves++;
			saves.setProperty("numberOfSaves", Integer.toString(numberOfSaves));
			saves.setProperty(Integer.toString(numberOfSaves),
					file.getAbsolutePath());
		} else
			System.out.println("not putten in Properties");

		try {
			FileOutputStream out = new FileOutputStream("saves.properties");
			saves.store(out, null);
			out.close();
			// Custom button text
			Object[] options = { "Ja", "Nein", };
			int n = JOptionPane.showOptionDialog(null,
					"Willst Du das Programm beenden?",
					"Drohendes Programmende", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
			if (n == 0)
				Main.kill();

		} catch (IOException ioe) {
		}

	}

}
