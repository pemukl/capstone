package capstone.menu.options;

import capstone.Main;
import capstone.Screen;

/**
 * @author marc
 *Toetet das Spiel bei Auswahl.
 */
public class KillOption extends Option {

	public KillOption(String name, String description) {
		super(name, description);
	}

	public KillOption(String name) {
		super(name, "");
	}

	@Override
	public Screen doAction() {
		Main.kill();
		return null;
	}

}
