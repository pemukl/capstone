package capstone.menu.options;

import capstone.Main;
import capstone.Screen;

import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc
 *Abstrakte Option. Hat einen Namen, eine Beschreibung
 *und eine Methode, die aufgerufen wird, wenn die Option ausgefuehrt werden soll.
 */
public abstract class Option {

	private final String name;
	private final String description;

	public Option(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public void print(int row, boolean isSelected) {

		if (isSelected) {
			Screen.print(getDescription(), true);
			Main.term.applyBackgroundColor(Terminal.Color.WHITE);
			Main.term.applyForegroundColor(Terminal.Color.BLACK);
		} else {
			Main.term.applyBackgroundColor(Terminal.Color.BLACK);
			Main.term.applyForegroundColor(Terminal.Color.WHITE);
		}

		Main.term.moveCursor(0, row);
		;
		String optionName = toString();

		try {
			for (char c : optionName.toCharArray())
				Main.term.putCharacter(c);
		} catch (IndexOutOfBoundsException e) {
		}

	}

	@Override
	public String toString() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public abstract Screen doAction();
}
