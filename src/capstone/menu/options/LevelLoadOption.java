package capstone.menu.options;

import java.io.File;

import javax.swing.JFileChooser;

import capstone.Main;
import capstone.Screen;
import capstone.game.Game;

/**
 * @author marc
 *Laedt ein Spiel bei Auswahl.
 */
public class LevelLoadOption extends Option {

	File saveFile;

	public LevelLoadOption(File saveFile, String name, String description) {
		super(name, description);
		this.saveFile = saveFile;
	}

	public LevelLoadOption(File saveFile, String name) {
		this(saveFile, name, "");
	}

	@Override
	public Screen doAction() {

		File tmpFile = saveFile;
		if (tmpFile == null) {
			JFileChooser fileChooser = new JFileChooser();
			int userSelection = fileChooser.showOpenDialog(null);
			if (userSelection == JFileChooser.APPROVE_OPTION)
				tmpFile = fileChooser.getSelectedFile();
		}
		Game game = null;
		if (tmpFile != null) {
			game = new Game(tmpFile);
			Main.getMainMenu().setGame(game);
		}
		return game;
	}

}
