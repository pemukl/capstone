package capstone.menu.options;

import capstone.Screen;

public class NextScreenOption extends Option {

	private Screen pointedScreen;

	public NextScreenOption(Screen pointedScreen, String name,
			String description) {
		super(name, description);
		this.pointedScreen = pointedScreen;
	}

	public NextScreenOption(Screen pointedScreen, String name) {
		this(pointedScreen, name, "");
	}

	@Override
	public Screen doAction() {
		return pointedScreen;
	}

}
