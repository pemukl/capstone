package capstone.menu;

import capstone.game.Game;
import capstone.menu.instructions.Instructions;
import capstone.menu.options.KillOption;
import capstone.menu.options.NextScreenOption;

public class MainMenu extends GenericMenu {

	public MainMenu() {
		super(null);
		name = "Hauptmenue";

		options.add(new NextScreenOption(new SaveScreen(this), "Speichern",
				"Ein Untermenue zum Speichen des aktuellen Spielstandes."));

		options.add(new NextScreenOption(new LoadScreen(this), "Levels Laden",
				"Ein Untermenue um ein Level zu laden."));

		options.add(new NextScreenOption(new Legend(this), "Legende",
				"Erlauterungen zu Symbolen."));

		options.add(new NextScreenOption(new Instructions(this), "Hilfe",
				"Erlauterungen zum Spiel."));

		options.add(new KillOption("Beenden", "Beendet das Programm"));
	}

	/**
	 * Fuegt ein Spiel zum Menue hinzu und zeigt es auch in den Options an.
	 * 
	 * @param game
	 *            das hinzu gefuegt wird
	 */
	public void setGame(Game game) {

		if (parent != null)
			options.remove(options.size() - 2);

		parent = game;
		options.add(options.size() - 1, new NextScreenOption(game,
				"Zurueck zum Spiel", "Setzt das aktuelle Spiel fort"));
	}

	public Game getGame() {
		return (Game) parent;
	}

}
