package capstone.menu;

import java.util.LinkedList;

import capstone.Main;
import capstone.Screen;
import capstone.menu.options.Option;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc Diese Klasse stellt ein abstraktes Menue dar. Sie kuemmert sich
 *         in erster Linie um Optionen.
 */
public abstract class GenericMenu extends Screen {
	protected String name;
	// Diese Variable sind die verfuegtbaren Optionen, die dann von den
	// Subklassen initiiert werden.
	protected LinkedList<Option> options;
	// Die Stelle der im Moment ausgewaehlten Option.
	protected int cursor;
	private static int row;

	public GenericMenu(Screen parent) {
		super(parent);
		row = 0;
		cursor = 0;
		options = new LinkedList<>();
	}

	@Override
	public Screen update(Key input) {
		if (input == null)
			return this;

		moveCusor(input);
		if (input.getKind() == Key.Kind.Enter)
			return doCursorOption();

		return this;
	}

	@Override
	public void show() {
		Main.term.clearScreen();
		Main.term.setCursorVisible(false);
		printName();
		printOptions();
	}

	@Override
	public void actOnResize() {
		print("Groeße veraendert.", Terminal.Color.RED);
		show();
	}

	private void printName() {
		row = 0;

		Main.term.moveCursor(0, row);
		Main.term.applyBackgroundColor(Terminal.Color.BLACK);
		Main.term.applyForegroundColor(Terminal.Color.WHITE);

		try {
			for (char c : ("---" + name).toCharArray())
				Main.term.putCharacter(c);
		} catch (IndexOutOfBoundsException e) {
		}

		row++;
	}

	private Screen doCursorOption() {
		Option selectedOption = options.get(cursor);

		Screen response = selectedOption.doAction();
		if (response != null)
			return response;
		return this;
	}

	private void printOptions() {

		if (options == null)
			return;

		for (int i = 0; i < options.size(); i++)
			printOption(i);
		row = options.size() + 1;

	}

	private void moveCusor(Key input) {
		if ((input.getKind() == Key.Kind.ArrowDown)
				&& (cursor < (options.size() - 1))) {
			cursor++;
			printOption(cursor - 1);
			printOption(cursor);
		}
		if ((input.getKind() == Key.Kind.ArrowUp) && (cursor > 0)) {
			cursor--;
			printOption(cursor + 1);
			printOption(cursor);
		}
	}

	private void printOption(int position) {
		options.get(position).print(position + 1, position == cursor);
	}

}
