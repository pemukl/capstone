package capstone.menu;

import capstone.Screen;
import capstone.game.boardMembers.BoardMember;
import capstone.game.boardMembers.DynamicObst;
import capstone.game.boardMembers.Entry;
import capstone.game.boardMembers.Exit;
import capstone.game.boardMembers.KeyM;
import capstone.game.boardMembers.Live;
import capstone.game.boardMembers.Player;
import capstone.game.boardMembers.StaticObst;
import capstone.game.boardMembers.Torch;
import capstone.game.boardMembers.Wall;
import capstone.menu.options.NextScreenOption;
import capstone.menu.options.TextOption;

public class Legend extends GenericMenu {
	Legend(Screen parent) {
		super(parent);
		name = "Legende";

		BoardMember member = new StaticObst();
		options.add(new TextOption(member.getSymbol(), "Statisches Hindernis",
				"Das sind Fallen, die Dich Leben kosten, wenn Du darauf laeufst"));

		member = new DynamicObst();
		options.add(new TextOption(
				member.getSymbol(),
				"Dynamisches Hindenis",
				"Das sind Monster, die sich bewegen koennen und Dich bei Beruehrung Leben kosten."));

		member = new Player();
		options.add(new TextOption(member.getSymbol(), "Spielfigur",
				"Das bist Du."));

		member = new KeyM();
		options.add(new TextOption(member.getSymbol(), "Schluessel",
				"Du brauchst einen Schluessel, um das Level verlassen zu koennen."));

		member = new Exit();
		options.add(new TextOption(member.getSymbol(), "Ausgang",
				"Durch den Ausgang kannst Du entkommen, sobald Du einen Schluessel hast."));

		member = new Entry();
		options.add(new TextOption(member.getSymbol(), "Eingang",
				"Hier wirst Du 'geboren' und kannst Deine Leben wieder auffuellen."));

		member = new Wall();
		options.add(new TextOption(member.getSymbol(), "Wand",
				"Waende stehen im Weg, esseiden Du bist im Cheatmodus."));

		member = new Live();
		options.add(new TextOption(member.getSymbol(), "Leben",
				"Wenn Du dieses Kraut einsammelst, bekommst Du 1 LP."));

		member = new Torch();
		options.add(new TextOption(member.getSymbol(), "Fackel",
				"Beleuchtet Umgebung in der Nacht und Monster kommen nicht durch."));

		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck ins Hauptmenue"));
	}
}
