package capstone.menu;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import capstone.game.Game;

public class LoadSaveScreen extends MainMenu {




	public LoadSaveScreen( GenericMenu genericMenu) {
		name = ("Spielstand laden");
		mother = genericMenu;
		Properties saves = new Properties();
		
		int numberOfSaves;
		try{
			saves.load(new FileInputStream("saves.properties"));
			if(saves.getProperty("numberOfSaves") == null)
				numberOfSaves = 0;
			else
				numberOfSaves = Integer.parseInt(saves.getProperty("numberOfSaves"));
		} catch (IOException ioe){
			numberOfSaves =0;
		}
		options = new String[numberOfSaves];
		
		for(int i=0; i < numberOfSaves; i++)
			options[i] = saves.getProperty(Integer.toString(i+1));

		show();
	}


	
	public MainMenu doCusorOption(int cursor) {
		Game.loadFile(options[cursor] + ".save");
		mother.hide();
		return mother;
		}

}
