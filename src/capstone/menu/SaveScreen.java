package capstone.menu;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import capstone.Screen;
import capstone.menu.options.LevelSaveOption;
import capstone.menu.options.NextScreenOption;

public class SaveScreen extends GenericMenu {

	public SaveScreen(Screen parent) {
		super(parent);
		name = "Spielstand Speichen";
		buildOptions();
	}

	@Override
	public void show() {
		options.clear();
		buildOptions();
		super.show();
	}

	private void buildOptions() {
		loadSavesFromProperties();
		options.add(new LevelSaveOption(null, "Andere",
				"Oeffnet Dateibrowser, um Datei auszusuchen"));
		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck ins Hauptmenue"));
	}

	/**
	 * baut die Optionen aus "saves.properties".
	 */
	private void loadSavesFromProperties() {

		Properties saveLocations = new Properties();

		int numberOfSaves;
		try {
			saveLocations.load(new FileInputStream("saves.properties"));
			if (saveLocations.getProperty("numberOfSaves") == null)
				numberOfSaves = 0;
			else
				numberOfSaves = Integer.parseInt(saveLocations
						.getProperty("numberOfSaves"));
		} catch (IOException ioe) {
			numberOfSaves = 0;
		}

		for (int i = numberOfSaves; i >= 1; i--) {
			String location = saveLocations.getProperty(Integer.toString(i));
			File save = new File(location);
			options.add(new LevelSaveOption(save, save.getName(), save
					.getAbsolutePath()));
		}
	}

}
