package capstone.menu;

import com.googlecode.lanterna.input.Key;

import capstone.Main;
import capstone.Screen;
import capstone.game.Game;

public class LoadFileScreen extends GenericMenu {

	private String fileName = null;	//Bisher eingegebener Dateiname




	public LoadFileScreen( Screen parent) {
		super(parent);
		name = ("Spielstand laden");
		options = null;
		show();
	}


	public void show() {
		clearPage();
		println("Gib einen Dateinamen ein.");

		Main.term.setCursorVisible(true);
		print("");		//curser wird in der neuen Zeile angezeigt
	}

	public MainMenu workOnInput(Key input) {
		if(prePocessInput(input) != null)
			return prePocessInput(input);

		if(input.getKind() == Key.Kind.Backspace){
			fileName = null;
			show();
			return this;
		}


		if(input.getKind() == Key.Kind.Enter){
			boolean foundFile;

			foundFile = Game.loadFile(fileName);

			if(foundFile){
				hide();
				return mother;
			}else{
				println("");
				println("Diese Datei existiert nicht, versuch es nochmal:");
				fileName=null;
				print("");
				return this;
			}
		}
		if(fileName == null)
			fileName = Character.toString(input.getCharacter());
		else
			fileName += Character.toString(input.getCharacter());

		print(fileName);
		return this;
	}


	@Override
	public State update(Key input) {
		// TODO Auto-generated method stub
		return null;
	}

}
