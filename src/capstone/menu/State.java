package capstone.menu;

import com.googlecode.lanterna.input.Key;

public abstract class State {
	protected State parent;
	private boolean isShown =false;
	
	public State(State parent) {
		this.parent = parent;
	}

	public State getNextState(Key input){
		if(!isShown){
			show();
			isShown = true;
		}
		
		State nextState = update(input);
		
		if(nextState != this)
			isShown = false;
		
		return nextState;
			
	}

	public abstract State update(Key input);
	
	public abstract void show();
	
}
