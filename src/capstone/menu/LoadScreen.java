package capstone.menu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import capstone.Screen;
import capstone.menu.options.LevelLoadOption;
import capstone.menu.options.NextScreenOption;

public class LoadScreen extends GenericMenu {

	private final String[] standardFiles = { "default.save",
			"level.properties", "level_small.properties",
			"level_big_dense.properties", "level_big_sparse.properties" };

	public LoadScreen(Screen parent) {
		super(parent);
		name = "Spielstand laden";
		buildOptions();
	}

	@Override
	public void show() {
		options.clear();
		buildOptions();
		super.show();
	}

	private void buildOptions() {
		loadSavesFromProperties();

		
		//sucht noch nach Standartdateien und fuegt sie evtl auch zu den Optionen hinzu
		for (String standardFile : standardFiles) {
			File save = new File(standardFile);
			if (save.exists())
				options.add(new LevelLoadOption(save, save.getName(), save
						.getAbsolutePath()));
		}
		
		//fuegt noch die Moeglichkeit hizu durch den Dateibrowser zu laden und zurueck zu gehen 
		options.add(new LevelLoadOption(null, "Andere",
				"Oeffnet Dateibrowser, um Datei auszusuchen"));
		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck ins Hauptmenue"));
	}

	/**
	 * fuegt zu den options die Dateien aus saves.properties hinzu, falls es welche gibt.
	 */
	private void loadSavesFromProperties() {
		
		

		Properties saveLocations = new Properties();

		int numberOfSaves;
		
		//laedt propertires
		try {
			saveLocations.load(new FileInputStream("saves.properties"));
			if (saveLocations.getProperty("numberOfSaves") == null)
				numberOfSaves = 0;
			else
				numberOfSaves = Integer.parseInt(saveLocations
						.getProperty("numberOfSaves"));
		} catch (IOException ioe) {
			numberOfSaves = 0;
		}

		
		//Geht die Anzahl Speicherstaende durch und loescht eintraege, falls die Dateien nicht existieren.
		//Dazu muessen alle weiteren Eintraege allerdings verschoben werden.
		for (int i = numberOfSaves; i >= 1; i--) {
			String location = saveLocations.getProperty(Integer.toString(i));
			File save = new File(location);
			if (save.exists())
				options.add(new LevelLoadOption(save, save.getName(), save
						.getAbsolutePath()));
			else {
				saveLocations.remove(Integer.toString(i));
				for (int j = i + 1; j <= numberOfSaves; j++)
					saveLocations.setProperty(Integer
							.toString(numberOfSaves - 1), saveLocations
							.getProperty(Integer.toString(numberOfSaves)));
				numberOfSaves -= 1;
				saveLocations.setProperty("numberOfSaves",
						Integer.toString(numberOfSaves));
			}
		}
		
		
		//speichert die modifizierte properties.
		try {
			FileOutputStream out = new FileOutputStream("saves.properties");
			saveLocations.store(out, null);
			out.close();
		} catch (IOException ioe) {
		}
	}

}
