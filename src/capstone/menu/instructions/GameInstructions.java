package capstone.menu.instructions;

import capstone.Screen;
import capstone.menu.GenericMenu;
import capstone.menu.options.NextScreenOption;
import capstone.menu.options.TextOption;

public class GameInstructions extends GenericMenu {

	public GameInstructions(Screen parent) {
		super(parent);
		name = "Erlauterungen zum Spiel";

		options.add(new TextOption(
				"Ziel des Spieles",
				"Ziel des Spiels ist es einen Schuessel einzusammeln und den Ausgang zu erreichen."));

		options.add(new TextOption("'LP'",
				"LP sind Deine Lebenspunkte, wenn Du keine mehr hast, ist das Spiel vorbei."));

		options.add(new TextOption(
				"Kraut",
				"Kraeuter werden zufaellig generiert, wenn Du eine Jungfraeuliche Spieldatei laedst."));

		options.add(new TextOption("Fallen",
				"Auf Fallen kannst Du Dich stellen, aber Du verlierst dabei ein Leben."));

		options.add(new TextOption("Cheatmodus",
				"Im Cheatmodus kannst Du keine Leben verlieren und durch Waende gehen."));

		options.add(new TextOption(
				"Tag und Nacht",
				"In der Nacht siehst Du nur diejenigen Felder, die mit Fackeln beleuchtet sind."));

		options.add(new TextOption("Schlaue Monster",
				"Je naeher Du einem Monster kommst, desto wahrscheinlicher kommt es zu Dir."));

		options.add(new TextOption("Monster fangen",
				"Fackeln sind sehr nuetzlich, um Monster einzusperren."));

		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck zur Hilfe."));

	}

}
