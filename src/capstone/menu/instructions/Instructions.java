package capstone.menu.instructions;

import capstone.Screen;
import capstone.menu.GenericMenu;
import capstone.menu.options.NextScreenOption;
import capstone.menu.options.TextOption;

public class Instructions extends GenericMenu {

	public Instructions(Screen parent) {
		super(parent);
		name = "Hilfe";

		options.add(new TextOption("Terminalgroeße",
				"Wird nur waehrend des Spiels aktiv kontrolliert!"));

		options.add(new NextScreenOption(new ControlInstructions(this),
				"Steuerung", "Erlaeuterung der Steuerung"));

		options.add(new NextScreenOption(new GameInstructions(this), "Spiel",
				"Erlaeuterung des Spiels"));

		options.add(new NextScreenOption(new ScreenInstructions(this),
				"Anzeige", "Erlaeuterung der Anzeige"));

		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck ins Hauptmenue"));

		/*
		 * options.add(new TextOption("","")); options.add(new
		 * TextOption("","")); options.add(new TextOption("",""));
		 * options.add(new TextOption("","")); options.add(new
		 * TextOption("","")); options.add(new TextOption("",""));
		 * options.add(new TextOption("","")); options.add(new
		 * TextOption("",""));
		 * 
		 * 
		 * 
		 * println(" "); println(""); println("------Steuerung"); println(" ");
		 * println(" "); println(" "); println(" "); println(" "); println(" ");
		 * println(" "); println("");
		 * 
		 * 
		 * println("------Menue");
		 * println("NEUES SPIEL: Hier kann man ein Testlevel initialisieren.");
		 * println("DATEI LADEN: Hier kann man eine beliebige Datei laden.");
		 * println(
		 * "SPEICHERN: Dateinamen eingeben und aktuelles Spiel speichern. (geht nur falls initialisiert)"
		 * ); println(
		 * "GESPEICHERTES LADEN: Hier koennen in 'saves.properties' vermerkte .save Dateien geladen werden."
		 * ); println("LEGENDE: Hier werden alle Symbole erklaert.");
		 * println("REST: selbsterklaerend");
		 */
	}

}
