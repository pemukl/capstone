package capstone.menu.instructions;

import capstone.Screen;
import capstone.menu.GenericMenu;
import capstone.menu.options.NextScreenOption;
import capstone.menu.options.TextOption;

public class ControlInstructions extends GenericMenu {

	public ControlInstructions(Screen parent) {
		super(parent);
		name = "-----Steuerung";

		options.add(new TextOption("ENTF",
				"Beendet das Programm in jeder Situation."));
		options.add(new TextOption("ESC",
				"Wechelt von Spiel ins Menue  und umgekehrt. Im Menue bringt es Dich zurueck."));
		options.add(new TextOption("PFEILTASTEN",
				"Damit navigierst Du die Figur. Im Menue zum Wechseln von Eintraegen."));
		options.add(new TextOption("ENTER",
				"Aktiviert Cheatmodus. Im Menue zur Auswahl eines Eintrages."));
		options.add(new TextOption("LEERTASTE",
				"Steckt eine Fackel in den Boden."));
		options.add(new TextOption("TAB",
				"Macht aus der Nacht den Tag und Fackeln bringen was!"));
		options.add(new TextOption("F1",
				"Leitet manuell eine Zentrierung des Feldes auf den Spieler ein."));
		options.add(new TextOption("F2",
				"Speichert undert dem Dateinamen 'default.save'."));
		options.add(new TextOption("F3",
				"Laedt die Datei 'default.save', falls vorhanden."));
		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck zur Hilfe"));

	}

}
