package capstone.menu.instructions;

import capstone.Screen;
import capstone.menu.GenericMenu;
import capstone.menu.options.NextScreenOption;
import capstone.menu.options.TextOption;

public class ScreenInstructions extends GenericMenu {

	public ScreenInstructions(Screen parent) {
		super(parent);
		name = "Erlauterungen zur Anzeige";

		options.add(new TextOption("LP", "LP werden oben links angezeigt."));
		options.add(new TextOption("Schluessel",
				"Schuessel wird falls vorhanden mit Position angezeigt. Sonst 'null'."));
		options.add(new TextOption("Fackeln",
				"Die Anzahl der verbleibenden Fackeln wird ebenfalls angezeigt."));
		options.add(new TextOption(
				"Nachrichten",
				"Nachrichten werden hier unten eingeblendet und verschwinden in der Regel nach 1 Sek."));
		options.add(new TextOption("Sogar in farbig",
				"Nachrichten haben je nach Situation versiedene Farben."));
		options.add(new NextScreenOption(parent, "Zurueck",
				"Fuehrt Dich zurueck zur Hilfe"));

	}

}
