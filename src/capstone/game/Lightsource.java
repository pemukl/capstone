package capstone.game;

import java.util.LinkedList;

import capstone.game.boardMembers.BoardMember;
import capstone.game.boardMembers.Symbol;

public abstract class Lightsource extends BoardMember {

	private final int radian;
	private GraphicsManager gManager;

	/**
	 * @param radian
	 *            ist der Radius der Lichtquelle (also die Staerke).
	 * @param pSymbol
	 *            ist das Symbol (geerbt von BoardMember)
	 * @param collectable
	 *            ob es einsammelbar ist (geerbt von BoardMember)
	 */
	public Lightsource(int radian, Symbol pSymbol, boolean collectable) {
		super(pSymbol, collectable);
		this.radian = radian;
	}

	/**
	 * Setzt seine Nachbarn auf die Updateliste des gManagers.
	 * 
	 * @param gManager
	 */
	public void flagNeighbours(GraphicsManager gManager) {
		this.gManager = gManager;
		for (Position pos : getNeighbours())
			gManager.flagPosition(pos);
	}

	private LinkedList<Position> getNeighbours() {
		LinkedList<Position> neighbours = new LinkedList<>();

		// zunaechst wird das Quadrat der Nachbarn betrachtet und dann werden
		// nur die genommen, die weniger als radian entfernt sind.
		for (int x = -radian; x <= radian; x++)
			for (int y = -radian; y <= radian; y++) {
				Position pos = getPos().getBoard().getPos(getPos().coor.x + x,
						getPos().coor.y + y);
				if ((Math.hypot(x, y) <= radian) && (pos != null))
					neighbours.add(pos);
			}

		return neighbours;
	}

	/**
	 * fuegt eine Lichtquelle zu den Nachbarn hinzu.
	 */
	protected void toggleOn() {
		for (Position pos : getNeighbours())
			pos.addLightsource(this);
	}

	/**
	 * Entfernt die Lichtquelle von den Nachbarn wieder.
	 */
	protected void toggleOff() {
		for (Position pos : getNeighbours())
			pos.removeLightsource(this);
		for (Position pos : getNeighbours())
			gManager.flagPosition(pos);
	}

	/**
	 * Gibt eine Lichtintensitaet zwischen 0 und 1
	 * in Abhaengigkeit der angegeben Koordinate zurueck.
	 * @param coor Koordinate des zu berechnenden Partners
	 * @return Intensitaet
	 */
	public Double getIntensity(Coordinate coor) {
		
		//zunaechst wird der Abstand auf das Intervall zwischen 0 und 1 geshrinkt.
		Double normed = 1.0 - (Math.hypot(getPos().coor.x - coor.x, getPos().coor.y
				- coor.y) / radian);

		//und dann noch ein Wenig modifiziert, damit der Verlauf schoen wird (durch ausprobieren).
		return 0.9 * ((1 - Math.exp(normed)) + (Math.E * normed));
	}

}
