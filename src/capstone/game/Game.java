package capstone.game;

import java.io.File;

import capstone.Main;
import capstone.Screen;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc Diese Klasse repraesentiert ein Spiel, welches anhand einer
 *         Datei gestartet wird. Sie ist ein Screen und muss somit anzeigbar und
 *         updatebar sein. Ihre essenziellen Member sind die Klasse zur
 *         Verwaltung der Anzeige und das eigentliche Spielfeld.
 */
public class Game extends Screen {

	public enum GameState {
		NOTINITIALIZED, RUNNING, WON, LOST, END, CHEATMODE
	}

	public GameState state = GameState.NOTINITIALIZED;
	private GraphicsManager gManager;
	private Board board;

	public Game(File file) {
		super(Main.getMainMenu());
		state = GameState.RUNNING;
		board = new Board(file, this);
		gManager = new GraphicsManager(board, board.getPlayer());
		board.setGManager(gManager);
		print("Spiel geladen: " + file.getName());
	}

	/**
	 * Verarbeitet die Zustaende WON, LOST und NOTINITIALIZED.
	 * 
	 * @return ob das Spiel laeuft
	 */
	private boolean isRunning() {
		switch (state) {
		case END:
			return false;
		case WON:
			return false;
		case LOST:
			return false;
		case NOTINITIALIZED:
			System.out.flush();
			System.err.println("Game not jet initialized!");
			return false;
		default:
			return true;
		}
	}

	/**
	 * Zeigt das Spielfeld an.
	 */
	@Override
	public void show() {
		if (state != GameState.NOTINITIALIZED)
			gManager.reBuild();

		switch (state) {
		case WON: // falls das Spiel gewonnen ist, wird eine Meldung ausgegeben
			// und beendet
			print("Das Spiel ist vorbei - Du hast gewonnen!", true);
			break;
		case LOST: // falls das Spiel verloren ist, wird eine Meldung ausgegeben
			// und beendet
			print("Das Spiel ist vorbei - Du hast verloren!", true);
			break;
		case NOTINITIALIZED: // falls das Spiel gewonnen ist, wird es direkt
			// beendet, um Fehler zu verhindern.
			System.out.flush();
			System.err.println("Game not jet initialized!");
		default:
		}
	}

	/**
	 * Speichert
	 * 
	 * @param fileName
	 *            Dateiname zum Speichern
	 * @return
	 */
	public boolean save(File file) {
		return board.save(file);
	}

	/*
	 * Diese Methode verarbeitet die verschiedenen Inputs und updated die
	 * Anzeige.
	 */
	@Override
	public Screen update(Key input) {

		if (state == GameState.NOTINITIALIZED){
			System.out.println("Kein Spiel initialisiert...");
			return parent;
		}


		//Ist ein Input gemacht worden, wird er auf die speziellen Befhle ueberprueft.
		if(input!= null)
			switch (input.getKind()) {

			case F1:
				gManager.reFocus();
				return this;

			case F2:
				File saveFile = new File("default.save");
				save(saveFile);
				break;

			case F3:
				File loadFile = new File("default.save");
				Main.getMainMenu().setGame(new Game(loadFile));
				return Main.getMainMenu().getGame();

			case Enter:
				if (state == GameState.CHEATMODE) {
					state = GameState.RUNNING;
					print("Cheatmodus deaktiviert");
					System.out.println("Cheatmode disabled.");
				} else {
					state = GameState.CHEATMODE;
					print("Cheatmodus aktiviert");
					System.out.println("Cheatmode enabled.");
				}
				break;

			case Tab:
				Position.toggleDay();
				gManager.reBuild();
				break;

			default:
				break;
			}

		// falls das Spiel nicht laeuft, wird sonst nichts mehr gemacht.
		if (!isRunning()) 
			return this;
		
		board.moveDynamics();

		// gibt es keinen Input, muss auch nichts verarbeitet werden.
		if (input != null) {
			board.updatePlayer(input);
		}
		gManager.refresh();
		return this;

	}

	@Override
	public void actOnResize() {
		gManager.flagResize();
		Screen.print("Groeße veraendert.", Terminal.Color.RED);
	}

}
