package capstone.game.boardMembers;

public class Air extends BoardMember {

	public Air() {
		super(new Symbol(' ', Color.BLACK, Color.BLACK), false);
	}

	@Override
	public boolean bump(Moveable bumper) {
		return true;
	}

}
