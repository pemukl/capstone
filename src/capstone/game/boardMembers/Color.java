package capstone.game.boardMembers;

public class Color {

	public static Color BLACK = new Color(0, 0, 0);
	public static Color WHITE = new Color(255, 255, 255);
	public static Color RED = new Color(255, 0, 0);
	public static Color CYAN = new Color(0, 255, 255);
	public static Color YELLOW = new Color(255, 255, 0);
	public static Color BLUE = new Color(0, 0, 255);

	public final int red;
	public final int green;
	public final int blue;

	public Color(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public Color(double r, double g, double b) {
		this.red = (int) r;
		this.green = (int) g;
		this.blue = (int) b;
	}

}
