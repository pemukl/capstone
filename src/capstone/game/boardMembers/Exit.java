package capstone.game.boardMembers;

import capstone.Main;
import capstone.Screen;
import capstone.game.Game;

public class Exit extends BoardMember {

	public Exit() {
		super(new Symbol('\u220f', Color.CYAN), false);
	}

	@Override
	public boolean bump(Moveable mvbl) {
		if (mvbl instanceof DynamicObst)
			return false;

		Player plr = (Player) mvbl;

		if (plr.getKey() == null) {
			System.out
					.println("Entry does not toggle a Win because of the missing Key.");
			Screen.print("Sorry, Du hast keinen Schluessel");
		} else {
			Main.getMainMenu().getGame().state = Game.GameState.WON;
			Screen.print("Das Spiel ist vorbei - Du hast gewonnen!", true);
		}
		return true;
	}

}
