package capstone.game.boardMembers;

import capstone.game.Lightsource;

public class Entry extends Lightsource {

	public Entry() {
		super(30, new Symbol('\u2302', Color.WHITE), false);
	}

	@Override
	public boolean bump(Moveable bumper) {
		if (bumper instanceof Player)
			((Player) bumper).refillhealth();

		return true;
	}

}
