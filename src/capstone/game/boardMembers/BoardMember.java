package capstone.game.boardMembers;

import capstone.game.Position;

/**
 * @author marc
 *Diese abstrakte Klasse repraesentiert Feldelemente.
 *Generell hat ein Feldelement ein Symbol und die Eigenschaft vom Spieler einsammelbar zu sein.
 *Desweiteren muss eine Subklasse spezifizieren, was getan wird, wenn ein bewegbares Feldelement versucht darauf zu laufen.
 */
public abstract class BoardMember {

	private Position mPos;
	public final Symbol mSymbol;
	public final boolean collectable;

	public BoardMember(Symbol pSymbol, boolean collectable) {
		mSymbol = pSymbol;
		this.collectable = collectable;
	}

	@Override
	public String toString() {
		return mSymbol.chr + mPos.toString();
	}

	/**
	 * @param bumper
	 * @return ob der Bumper sich darauf absetzen darf.
	 */
	public abstract boolean bump(Moveable bumper);

	public void setPosition(Position pPos) {
		mPos = pPos;
	}
	
	public Position getPos() {
		return mPos;
	}

	public Symbol getSymbol() {
		return mSymbol;
	}

}
