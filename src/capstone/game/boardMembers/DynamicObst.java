package capstone.game.boardMembers;

import java.util.Random;

import capstone.game.Coordinate;

import com.googlecode.lanterna.input.Key;

public class DynamicObst extends Moveable {

	private int round = 0;
	private final int speed = 20; // high number is slower

	public DynamicObst() {
		super(new Symbol('\u06DE', new Color(255, 0, 0)), false);
	}

	private Coordinate calculateDest(Coordinate playerCoor) {
		Random rand = new Random();
		double randX = 0.0;
		double randY = 0.0;
		int x = 0;
		int y = 0;
		int diffX = playerCoor.x - getPos().coor.x;
		int diffY = playerCoor.y - getPos().coor.y;
		
		double distRand = rand.nextDouble()-1;
		distRand = distRand+ 1/Math.pow(Math.hypot(diffX, diffY), 0.6);
		
		
		if (0> distRand){
			
			int randInt = rand.nextInt(4);
			switch(randInt){
			case 0:
				x=1;
				break;
			case 1:
				x=-1;
				break;
			case 2:
				y=1;
				break;
			case 3:
				y=-1;
				break;
			}

		} else {

			if (diffX != 0) {
				randX = (1 / (double) diffX) * rand.nextDouble();
			}

			if (diffY != 0) {
				randY = (1 / (double) diffY)* rand.nextDouble();
			}

			if (Math.abs(randX) > Math.abs(randY)) {
				x = (int) Math.signum(randX);
			} else {
				y = (int) Math.signum(randY);
			}
			
		}


		Coordinate newCoor = new Coordinate(getPos().coor.x + x, getPos().coor.y + y);
		return newCoor;
	}

	@Override
	public boolean bump(Moveable mvbl) {
		if (mvbl instanceof Player)
			((Player) mvbl).damage();

		return false;
	}

	@Override
	public Coordinate getDest(Coordinate playerCoor, Key input) {
		if (round < speed) {
			round++;
			return getPos().coor;
		}
		round = 0;
		return calculateDest(playerCoor);
	}



}
