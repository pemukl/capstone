package capstone.game.boardMembers;

public class StaticObst extends BoardMember {

	public StaticObst() {
		super(new Symbol('\u00A4', Color.RED), false);
	}

	@Override
	public boolean bump(Moveable mvbl) {
		if (mvbl instanceof Player)
			((Player) mvbl).damage();
		return true;
	}

}
