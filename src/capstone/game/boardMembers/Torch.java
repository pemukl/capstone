package capstone.game.boardMembers;

import capstone.Screen;
import capstone.game.Lightsource;

import com.googlecode.lanterna.terminal.Terminal;

public class Torch extends Lightsource {

	public Torch() {
		super(20, new Symbol('\uFD3D', Color.YELLOW), true);
	}

	@Override
	public boolean bump(Moveable bumper) {
		if (bumper instanceof DynamicObst)
			return false;
		Screen.print("Fackel gefunden.", Terminal.Color.YELLOW);
		Player plr = (Player) bumper;
		plr.giveTorch();
		toggleOff();
		return true;
	}

}
