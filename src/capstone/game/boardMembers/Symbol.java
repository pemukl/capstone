package capstone.game.boardMembers;

public class Symbol {

	public final char chr;
	public final Color clr;
	public final Color bgrd;

	public Symbol(char pChar, Color pColor) {
		clr = pColor;
		chr = pChar;
		bgrd = new Color(0, 0, 0);
	}

	public Symbol(char pChar, Color pColor, Color pBackground) {
		clr = pColor;
		chr = pChar;
		bgrd = pBackground;
	}

	/**
	 * Gibt ein neues Symbol mit angepassten Lichtwerten zurueck
	 * @param brightness zwischen 0 (schwarz) und 1 (die jeweilige Farbe).
	 * @return das dunklere Symbol
	 */
	public Symbol scaleColors(Double brightness) {
		Color bgrd = new Color(this.bgrd.red * brightness, this.bgrd.green
				* brightness, this.bgrd.blue * brightness);
		Color clr = new Color(this.clr.red * brightness, this.clr.green
				* brightness, this.clr.blue * brightness);

		return new Symbol(chr, clr, bgrd);
	}

}
