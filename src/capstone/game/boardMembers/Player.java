package capstone.game.boardMembers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import capstone.Main;
import capstone.Screen;
import capstone.game.Coordinate;
import capstone.game.Game;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc
 *Der Spieler ist ebenfalls ein BoardMember und insbesondere ein Moveable.
 *Er hat Leben, evtl einen Schluessel und Fackeln und kuemmert sich im Wesentlichen um diese Attribute.
 *
 *
 */
public class Player extends Moveable {

	final int startLives = 30;
	private int torches = 30;
	private int mLives;
	private KeyM mKey;
	//Wird auf true gesetzt, falls eine Info sich aendert, damit der Displaymanger weiß, dass er sie neu anzeigen muss.
	private boolean haveInfosChanged;

	public Player() {
		super(new Symbol('\u263A', Color.WHITE), false);
		mLives = startLives;
		haveInfosChanged = true;
	}

	public int getLives() {
		return mLives;
	}

	public void setLives(int lives) {
		mLives = lives;
		haveInfosChanged = true;
	}

	public KeyM getKey() {
		return mKey;
	}


	/**
	 * Fuegt dem Spieler schaden zu, wenn er noch welche hat.
	 * Wenn nicht, versucht er ein Soundfile abzuspielen und setzt den Status des Spiels auf LOST.
	 * Falls das Spiel im Cheatmodus ist, passiert nichts.
	 */
	public void damage() {


		if(mLives>0) {	


			if (Main.getMainMenu().getGame().state != Game.GameState.CHEATMODE){
				mLives--;
				Screen.print("Du wurdest verletzt!", Terminal.Color.RED);
				System.out.println("Player Damaged. Lives: " + mLives);
				haveInfosChanged = true;
			}else{
				Screen.print("Du mieser Cheater...", Terminal.Color.RED);
				System.out.println("Player took no harm...");
			}
		}

		if (mLives <= 0){
			toggleLostSound();
			Main.getMainMenu().getGame().state = Game.GameState.LOST;
			Main.getMainMenu().getGame().show();
		}
	}

	/**
	 * Versucht Sounddatei abzuspielen.
	 */
	private void toggleLostSound() {
		try {
			InputStream ns = Player.class.getResourceAsStream("showdown.wav");
			Clip player = AudioSystem.getClip();
			AudioInputStream as = AudioSystem.getAudioInputStream(ns);
			player.open(as);
			player.start();

		} catch (Exception e) {
			System.out.println("Konnte diesen Soundfile nicht abspielen.");
		}		
	}

	public void heal(int amount) {
		mLives += amount;
		haveInfosChanged = true;
	}

	/* (non-Javadoc)
	 * @see capstone.game.boardMembers.Moveable#getDest(capstone.game.Coordinate, com.googlecode.lanterna.input.Key)
	 * Uebersetzt einen input in eine Zielkoordinate.
	 */
	@Override
	public Coordinate getDest(Coordinate playerCoor, Key input) {
		if (input.getKind() == Key.Kind.ArrowDown)
			return new Coordinate(playerCoor.x, playerCoor.y + 1);
		if (input.getKind() == Key.Kind.ArrowUp)
			return new Coordinate(playerCoor.x, playerCoor.y - 1);
		if (input.getKind() == Key.Kind.ArrowLeft)
			return new Coordinate(playerCoor.x - 1, playerCoor.y);
		if (input.getKind() == Key.Kind.ArrowRight)
			return new Coordinate(playerCoor.x + 1, playerCoor.y);

		return null;
	}


	/* (non-Javadoc)
	 * @see capstone.game.boardMembers.BoardMember#bump(capstone.game.boardMembers.Moveable)
	 * gibt an, was passiert, wenn ein anderer Moveable (also ein DynamicObst, da es eh nur einen Spieler gibt) gegen ihn lauft
	 */
	@Override
	public boolean bump(Moveable bumper) {
		if(bumper instanceof DynamicObst)
			damage();
		//man kann nicht in den Player rein laufen, also:
		return false;
	}


	public void refillhealth() {
		if (mLives < startLives) {
			mLives = startLives;
			Screen.print("Willkommen Daheim. Du hast wieder volle Leben.");
			System.out.println("Lives regenerated at Entry.");
		}
		haveInfosChanged = true;
	}

	public void setKey(KeyM key) {
		mKey = key;
		haveInfosChanged = true;
	}

	public void giveTorch() {
		torches++;
		haveInfosChanged = true;
	}

	public void removeTorch() {
		torches--;
		haveInfosChanged = true;
	}

	public int getTorches() {
		return torches;
	}

	public void setTorches(int torches) {
		this.torches = torches;
		haveInfosChanged = true;
	}

	public boolean haveInfosChanged() {
		if(!haveInfosChanged)
			return false;
		else{
			haveInfosChanged = false;
			return true;
		}
	}

}
