package capstone.game.boardMembers;

import capstone.Main;
import capstone.game.Game;

public class Wall extends BoardMember {

	public Wall() {
		super(new Symbol(' ', Color.BLUE, Color.BLUE), false);
	}

	@Override
	public boolean bump(Moveable bumper) {

		if ((Main.getMainMenu().getGame().state == Game.GameState.CHEATMODE)
				&& (bumper instanceof Player))
			return true;

		return false;

	}

}
