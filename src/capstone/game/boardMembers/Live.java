package capstone.game.boardMembers;

import capstone.Screen;

import com.googlecode.lanterna.terminal.Terminal;

public class Live extends BoardMember {

	public Live() {
		super(new Symbol('\u2663', Color.CYAN), true);
	}

	@Override
	public boolean bump(Moveable bumper) {
		if (bumper instanceof DynamicObst)
			return true;

		Screen.print("Du hast ein Leben gefunden!", Terminal.Color.CYAN);

		Player plr = (Player) bumper;
		plr.heal(1);
		return true;
	}

}
