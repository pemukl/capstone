package capstone.game.boardMembers;

import capstone.game.Coordinate;

import com.googlecode.lanterna.input.Key;

public abstract class Moveable extends BoardMember {

	private BoardMember underlying = new Air();

	public Moveable(Symbol pSymbol, boolean collectable) {
		super(pSymbol, collectable);
	}

	public BoardMember getUnderlying() {
		return underlying;
	}

	public void setUnderlying(BoardMember underlying) {
		this.underlying = underlying;
	}

	public abstract Coordinate getDest(Coordinate playerCoor, Key input);

}
