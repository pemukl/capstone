package capstone.game.boardMembers;

import capstone.Screen;

public class KeyM extends BoardMember {

	public final boolean collectable = true;

	public KeyM() {
		super(new Symbol('\u2640', Color.YELLOW), true);
	}

	@Override
	public boolean bump(Moveable bumper) {
		if (bumper instanceof DynamicObst)
			return true;

		Player plr = (Player) bumper;
		plr.setKey(this);
		Screen.print("Schluessel gefunden!");
		System.out.println("Key found.");
		return true;
	}

}
