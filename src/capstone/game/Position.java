package capstone.game;

import java.util.LinkedList;

import com.googlecode.lanterna.terminal.Terminal.Color;

import capstone.Screen;
import capstone.game.boardMembers.BoardMember;
import capstone.game.boardMembers.Player;
import capstone.game.boardMembers.Symbol;

/**
 * @author marc
 * Diese Klasse repraesentiert eine Stelle auf dem Spielfeld.
 * Sie hat einen BoardMember, der sie gerade besetzt und kuemmert sich darum,
 * sein Symbol mit angepassten Lichtverhaeltnissen weiterzureichen.
 * Ebenenfalls hat jede Position eine Koordinate im Feld, an der sie sitzt.
 * Die statische Variable isDay ermoeglicht es, allen Positionen zu sagen, dass sie die Symbole in voller Intensitaet weitergeben sollen.
 */
public class Position {
	private Board board;
	private Double illumination = 0.0;
	private LinkedList<Lightsource> lightsources;
	public final Coordinate coor;
	public BoardMember member;
	private static boolean isDay = false;

	/**
	 * Einer Position muss am Anfang gesagt werden, auf welchem Spielfeld und an welcher Position sie sitzt.
	 * @param coor
	 * @param board
	 */
	public Position(Coordinate coor, Board board) {
		this.board = board;
		this.coor = coor;
		//Die lightsources werden auch schonmal instanziiert.
		lightsources = new LinkedList<>();
	}

	/**
	 * Berechnet die Beleuchtung anhand der anliegenden Lichtquellen.
	 * 
	 */
	private void calculateIllumination() {
		illumination = 0.0;

		for (Lightsource source : lightsources)
			illumination += source.getIntensity(coor);

	}

	@Override
	public String toString() {
		return "[" + coor.x + "|" + coor.y + "]";
	}

	public BoardMember getMember() {
		return member;
	}

	public void setMember(BoardMember pMember) {
		member = pMember;
	}

	/**
	 * Dies gibt das Symbol des Members mit gewichteter Helligkeit zurueck.
	 * (Falls es Tag ist mit voller Helligkeit.
	 *  @return ist das anzuzeigende Symbol.
	 */
	public Symbol getSymbol() {
		Double brightness = illumination;

		if (isDay || (member instanceof Player) || (illumination > 1.0))
			brightness = 1.0;

		return member.mSymbol.scaleColors(brightness);
	}

	public void addLightsource(Lightsource light) {
		lightsources.add(light);
		calculateIllumination();
	}

	public void removeLightsource(Lightsource light) {
		lightsources.remove(light);
		calculateIllumination();
	}

	public Board getBoard() {
		return board;
	}

	/**
	 * Fuer alle Positionen wird es gleichzeitig Tag, deshalb ist diese Methode
	 * statisch.
	 */
	public static void toggleDay() {
		isDay = !isDay;

		if (isDay) {
			System.out.println("It became day.");
			Screen.print("Es ist Tag geworden.", Color.YELLOW);
		} else {
			System.out.println("It became night.");
			Screen.print("Es ist Nacht geworden.", Color.MAGENTA);
		}

	}

}
