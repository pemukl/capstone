package capstone.game;

import java.util.LinkedList;

import com.googlecode.lanterna.terminal.Terminal;

import capstone.Main;
import capstone.game.boardMembers.Player;

/**
 * Diese Klasse kuemmert sich um Alles, was Grafikausgabe waehrend des laufenden
 * Spiels ist.
 * 
 * @author Marc Schneider
 */
public class GraphicsManager {

	private Board board;
	// Diese Variable wurde eingefuehrt, um einen doppelten zugriff auf toUpdate durch den ResizeListener zu vermeiden.
	private boolean resizeIsFlagged;
	// zuletzt mit checkFocus(Position center) ueberprueftes center.
	private Coordinate lastCheckedFocus;
	// Felder, die bein naechsten Refresh geupdatet werden.
	private LinkedList<Position> toUpdate; 
	// Feld, welches oben links angezeigt wird.
	private Coordinate cornerCoor; 
	// Spieler, dessen Infos angezeigt werden
	private Player player; 

	/**
	 * initialisiert den Graphicsmanager auf einem Board.
	 */
	public GraphicsManager(Board pBoard) {
		toUpdate = new LinkedList<>();
		cornerCoor = new Coordinate(0, 0);
		lastCheckedFocus = null;
		board = pBoard;
	}

	/**
	 * initialisiert den Graphicsmanager komplett neu und setzt gleich den
	 * Player.
	 * 
	 * @param plr
	 *            der gesetzt werden soll.
	 */
	public GraphicsManager(Board board, Player plr) {
		this(board);
		setPlayer(plr);
	}

	/**
	 * Setzt die Anzeige auf den aktuellsten Stand und passt sichtbaren
	 * Ausschnitt an.
	 */
	public void refresh() {
		
		if (resizeIsFlagged)
			reFlagAll();
		resizeIsFlagged = false;
		
		if(player.haveInfosChanged())
			printInfos();
		
		
		checkFocus(player.getPos().coor);
		
		
		// Hier werden die vorher geflagten Felder im Fensterauschnitt geupdatet.
		for (Position pos : toUpdate) {
			if (!isInRange(pos.coor))
				continue;
			updatePosition(pos);
		}
		toUpdate.clear();
	}

	public void flagResize() {
		lastCheckedFocus = null;
		resizeIsFlagged = true;
	}

	/**
	 * Ordnet an, die uebergebene Position beim naechsten refresh zu updaten.
	 * 
	 * @param pos
	 *            , die refresht werden soll
	 */
	public void flagPosition(Position pos) {
		toUpdate.add(pos);
	}

	/**
	 * @param coor
	 *            Position, die ueberprueft werden soll
	 * @return Ob die Position im aktuellen Sichtbereich liegt.
	 */
	private boolean isInRange(Coordinate coor) {

		if (coor == null)
			return false;

		if ((coor.x < cornerCoor.x)
				|| (coor.x > (cornerCoor.x + Main.term.getTerminalSize()
						.getColumns())))
			return false;
		if ((coor.y < cornerCoor.y)
				|| (coor.y > ((cornerCoor.y + Main.term.getTerminalSize()
						.getRows()) - 3)))
			return false;
		// drei wurden abgezogen, da in der obersten und untersten Zeile
		// Infos/Nachrichten angezeigt werden.

		return true;
	}

	/**
	 * @param pos
	 *            , die neu angezeigt werden soll
	 */
	private void updatePosition(Position pos) {
		Main.term.moveCursor(pos.coor.x - cornerCoor.x,
				(pos.coor.y - cornerCoor.y) + 1);

		Main.term.applyForegroundColor(pos.getSymbol().clr.red,
				pos.getSymbol().clr.green, pos.getSymbol().clr.blue);
		Main.term.applyBackgroundColor(pos.getSymbol().bgrd.red,
				pos.getSymbol().bgrd.green, pos.getSymbol().bgrd.blue);
		try {
			Main.term.putCharacter(pos.getSymbol().chr);
		} catch (IndexOutOfBoundsException e) {
		}
	}

	/**
	 * Diese Methode zentriert, falls noetig, den angezeigten Bildausschnitt
	 * neu.
	 * 
	 * @param center
	 *            Position, auf der zentriert werden soll.
	 */
	private void checkFocus(Coordinate center) {
		
		//Falls dieses center schonmal gecheckt wurde, macht er nichts.
		if ((center == null) || (center == lastCheckedFocus))
			return;
		
		boolean xChanged = ((lastCheckedFocus == null) || (center.x != lastCheckedFocus.x));
		boolean yChanged = ((lastCheckedFocus == null) || (center.y != lastCheckedFocus.y));

		if (xChanged
				&& ((center.x < (cornerCoor.x + 1)) || (center.x > ((cornerCoor.x + Main.term
						.getTerminalSize().getColumns()) - 2)))) {
			reFocusX(center);
			reFlagAll();
		}
		if (yChanged
				&& ((center.y < (cornerCoor.y + 1)) || (center.y > ((cornerCoor.y + Main.term
						.getTerminalSize().getRows()) - 4)))) {
			reFocusY(center);
			reFlagAll();
		}
		lastCheckedFocus = center; 
		// speichert die Mitte, damit er es beim naechsten Mal nicht nichmal checken muss
		// (Wenn der Spieler sich am Rand befindet, fuehrt das zu
		// Performanceverlust)
	}

	/**
	 * Ordnet eine Zentrierung auf die Spielfigur an.
	 */
	public void reFocus() {
		Coordinate center = player.getPos().coor;

		System.out.println("reSize on: x=" + center.x + ", y=" + center.y);

		reFocusX(center);
		reFocusY(center);
		reFlagAll();
	}

	/**
	 * Laedt das gesamte Spielfeld neu.
	 */
	public void reBuild() {
		Main.term.clearScreen();
		Main.term.setCursorVisible(false);
		printInfos();
		reFlagAll();
		refresh();
	}

	/**
	 * Zentriert die Y-Achse.
	 * 
	 * @param center
	 *            auf das zentriert weden soll
	 */
	private void reFocusY(Coordinate center) {

		int y = center.y - ((Main.term.getTerminalSize().getRows() - 2) / 2);

		if (y > ((board.height - Main.term.getTerminalSize().getRows()) + 1))
			y = (board.height - Main.term.getTerminalSize().getRows()) + 1;
		if (y < 0)
			y = 0;

		cornerCoor = new Coordinate(cornerCoor.x, y);
	}

	/**
	 * Zentriert die X-Achse.
	 * 
	 * @param center
	 *            auf das zentriert weden soll
	 */
	private void reFocusX(Coordinate center) {

		int x = center.x - (Main.term.getTerminalSize().getColumns() / 2);

		if (x > (board.width - Main.term.getTerminalSize().getColumns()))
			x = board.width - Main.term.getTerminalSize().getColumns();
		if (x < 0)
			x = 0;

		cornerCoor = new Coordinate(x, cornerCoor.y);
	}

	/**
	 * ordnet an, alle sichbaren Felder beim naechsten refresh zu updaten.
	 */
	private void reFlagAll() {
		board.flagAll(cornerCoor, new Coordinate(cornerCoor.x
				+ Main.term.getTerminalSize().getColumns(), cornerCoor.y
				+ Main.term.getTerminalSize().getRows()));
	}

	/**
	 * Zeigt in der obersten Zeile LP und Key an.
	 */
	private void printInfos() {

		String infos = "LP:" + player.getLives() + "\t Key:" + player.getKey()
				+ "\t Torches:" + player.getTorches() + "\t  ";

		Main.term.moveCursor(0, 0);
		Main.term.applyBackgroundColor(Terminal.Color.BLACK);
		Main.term.applyForegroundColor(Terminal.Color.WHITE);
		try {
			for (char c : infos.toCharArray())
				Main.term.putCharacter(c);
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}
