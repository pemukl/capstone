package capstone.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;

import capstone.Screen;
import capstone.game.boardMembers.Air;
import capstone.game.boardMembers.BoardMember;
import capstone.game.boardMembers.DynamicObst;
import capstone.game.boardMembers.Entry;
import capstone.game.boardMembers.Exit;
import capstone.game.boardMembers.KeyM;
import capstone.game.boardMembers.Live;
import capstone.game.boardMembers.Moveable;
import capstone.game.boardMembers.Player;
import capstone.game.boardMembers.StaticObst;
import capstone.game.boardMembers.Torch;
import capstone.game.boardMembers.Wall;

import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author marc
 * 
 *         Diese Klasse repraesentiert das eigentliche Spielfeld.
 *         Sie hat im Wesentlichen Positionen, den Spieler und die DynamischenHindernisse, und die Lichtquellen zu verwalten.
 */
public class Board {

	private Position[][] positions;
	private GraphicsManager gManager;
	public int height;
	public int width;
	private Player player;
	private LinkedList<DynamicObst> mDynamics;
	private LinkedList<Lightsource> Lightsources;

	/**
	 * Ein Spielfeld wirrd anhand einer Datei initialisiert.
	 * 
	 * @param file
	 */
	public Board(File file, Game game) {
		mDynamics = new LinkedList<>();
		Lightsources = new LinkedList<>();
		load(file, game);
	}

	/**
	 * Um sich anzeigen zu koennen, braucht das Feld einen GraphicsManager.
	 * 
	 * @param gManager
	 */
	public void setGManager(GraphicsManager gManager) {
		this.gManager = gManager;
		
		//Da wir nun einen GraphicsManager zum ausgeben haben, koennen wir auch die Fackeln platzieren.
		for (Lightsource source : Lightsources)
			source.flagNeighbours(gManager);
		gManager.reFocus();

	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public Position getPos(int x, int y) {
		if ((x < 0) || (x >= positions.length) || (y < 0)
				|| (y >= positions[0].length))
			return null;
		return positions[x][y];
	}

	/**
	 * Flagt alle Positionen eines Bereichs auf dem gManager.
	 * 
	 * @param cornerCoor
	 *            ist die obere, linke Coordinate des zu flaggenden Bereichs
	 * @param coordinate
	 *            ist die untere, rechte Coordinate des zu flaggenden Bereichs
	 */
	public void flagAll(Coordinate cornerCoor, Coordinate coordinate) {
		for (int y = cornerCoor.y; (y < height) && (y < coordinate.y); y++)
			for (int x = cornerCoor.x; (x < width) && (x < coordinate.x); x++)
				gManager.flagPosition(positions[x][y]);
	}

	/**
	 * Setzt einen neuen Player auf den uebergebenen Eingang.
	 * 
	 * @param entry
	 */
	private void initPlayer(Entry entry) {
		if (entry == null) {
			System.out.flush();
			System.err.println("No Entry found! No Player Initialized");
		} else {
			player = new Player();
			Position pos = positions[entry.getPos().coor.x][entry.getPos().coor.y];
			player.setPosition(pos);
			player.setUnderlying(new Entry());
			pos.setMember(player);
		}

	}

	/**
	 * ueberprueft den Input und bewegt den Player bzw. setzt eine Fackel unter
	 * ihn.
	 * 
	 * @param input
	 */
	public void updatePlayer(Key input) {

		if (input.getCharacter() == ' ')
			if ((player.getUnderlying() instanceof Air)
					&& (player.getTorches() > 0)) {
				Screen.print("Fackel platziert.", Terminal.Color.YELLOW);
				Torch torch = new Torch();
				torch.setPosition(player.getPos());
				player.setUnderlying(torch);
				torch.toggleOn();
				torch.flagNeighbours(gManager);
				player.removeTorch();
			}

		Coordinate dest = player.getDest(player.getPos().coor, input);
		move(player, dest);
	}

	/**
	 * Dynamische Hindernisse werden versetzt.
	 */
	public void moveDynamics() {
		for (DynamicObst dyn : mDynamics) {
			Coordinate dest = dyn.getDest(player.getPos().coor, null);
			move(dyn, dest);
		}
	}

	/**
	 * Bewegt einen Moveable an die Position, die sich an der Coordinate
	 * befindet. Insbesondere wird der vorher da gewesene Member gebumpt. Falls
	 * er den Moveable zuruuechstoeßt (false), wird der Moveable nicht bewegt.
	 * 
	 * @param toMove
	 * @param dest
	 */
	private void move(Moveable toMove, Coordinate dest) {
		if (dest == null)
			return;
		if (isInRange(dest)) {
			Position destination = positions[dest.x][dest.y];
			if (destination.member.bump(toMove))
				place(destination, toMove);
		}
	}

	/**
	 * Die unter dem Moveable liegenden Members werden aktualisiert, der
	 * Moveable bewegt und einsammelbare Members eingesammelt.
	 * 
	 * @param pos
	 * 		Position, auf die toMove gesetzt werden soll
	 * @param toMove
	 * 		Zu bewegender Moveable.
	 */
	private void place(Position pos, Moveable toMove) {
		if ((pos.coor.x >= positions.length) || (pos.coor.x < 0)
				|| (pos.coor.y >= positions[0].length) || (pos.coor.y < 0))
			return;

		Position oldPos = toMove.getPos();
		oldPos.setMember(toMove.getUnderlying());
		gManager.flagPosition(oldPos);

		BoardMember underlying = pos.getMember();
		if ((toMove instanceof Player) && pos.getMember().collectable)
			underlying = new Air();

		toMove.setPosition(pos);
		toMove.setUnderlying(underlying);
		pos.setMember(toMove);
		gManager.flagPosition(pos);
	}

	private boolean isInRange(Coordinate dest) {
		if ((dest.y >= positions[0].length) || (dest.y < 0))
			return false;
		if ((dest.x >= positions.length) || (dest.x < 0))
			return false;
		return true;
	}

	public Player getPlayer() {
		return player;
	}

	/**
	 * Diese Methode laedt das Spielfeld aus einer Datei.
	 * 
	 * @param file
	 * @return ob es geklappt hat
	 */
	public boolean load(File file, Game game) {
		System.out.println("Initializing from File: " + file);
		Screen.print("Lade Spiel: " + file.getName() + " Bitte warten...");

		Properties fieldProp = new Properties();

		try {
			fieldProp.load(new FileInputStream(file));
		} catch (IOException ioe) {
			System.out.flush();
			System.err.println("The file " + file
					+ " does not exist. Nothing loaded.");
			return false;
		}

		height = Integer.parseInt(fieldProp.getProperty("Height"));
		width = Integer.parseInt(fieldProp.getProperty("Width"));
		positions = new Position[width][height];

		// Da wir nicht bei jedem Ladevorgang neue Leben generieren wollen,
		// muessen wir feststellen, ob es schon welche gibt.
		boolean generateLives;
		if (fieldProp.getProperty("livesAlreadyGenerated") == null)
			generateLives = true;
		else
			generateLives = false;

		// In dieser Variable wird beim Laden ein Eingang festgehalten.
		Entry entry = null;
		// und hier ein eventueller Player.
		player = null;

		// Die Properties Datei wird mit zwei For-Schleifen nach BoardMembers
		// abgesucht.
		// Und die positions[][] werden initialisiert.
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {

				Coordinate coor = new Coordinate(x, y);
				Position pos = new Position(coor, this);
				positions[x][y] = pos;
				String str = fieldProp.getProperty(x + "," + y);
				BoardMember mbr = translateToMember(str, generateLives);
				mbr.setPosition(pos);
				positions[x][y].setMember(mbr);

				// nun werden noch verschiedene besondere Members abgefangen.
				if (mbr instanceof Lightsource) {
					Lightsources.add((Lightsource) mbr);
					if ((entry == null) && (mbr instanceof Entry))
						entry = (Entry) mbr;
				}

				if (mbr instanceof DynamicObst)
					mDynamics.add((DynamicObst) mbr);

				if (mbr instanceof Player)
					player = (Player) mbr;
			}

		// Falls kein Player gefunden wurde, wird er initialisiert.
		if (player == null)
			initPlayer(entry);
		// sonst werden seine Eigenschaften aus der Properties geladen.
		else {
			
			//zuerst die Leben
			String lives = fieldProp.getProperty("Lives");
			if (lives != null){
				int intLives = Integer.parseInt(lives);
				player.setLives(intLives);
				if(intLives <= 0)
					game.state = Game.GameState.LOST;
			}
			
			//Dann die Fackeln
			String torches = fieldProp.getProperty("Torches");
			if (torches != null)
				player.setTorches(Integer.parseInt(torches));

			//Der Schluessel
			if ((fieldProp.getProperty("KeyX") != null)
					&& (fieldProp.getProperty("KeyY") != null)) {
				KeyM key = new KeyM();
				Coordinate coor = new Coordinate(Integer.parseInt(fieldProp
						.getProperty("KeyX")), Integer.parseInt(fieldProp
								.getProperty("KeyY")));
				key.setPosition(new Position(coor, this));
				player.setKey(key);
			}
			
			//Den Boardmember unter dem Spieler
			if (fieldProp.getProperty("Underlying") != null) {
				BoardMember underlie = translateToMember(
						fieldProp.getProperty("Underlying"), false);

				//eine Fackel wird aufgehoben, wenn der Player darauf steht.
				if (underlie instanceof Torch) {
					player.giveTorch();
					player.setUnderlying(new Air());
				} else
					player.setUnderlying(underlie);

				//Falls er auf einem Ausgang steht und einen Schluessel hat, hat er gewonnen.
				if (underlie instanceof Exit && player.getKey() != null)
					game.state = Game.GameState.WON;

			} else
				player.setUnderlying(new Air());
		}

		// nun werden noch die Lichtquellen aktualisiert
		for (Lightsource source : Lightsources)
			source.toggleOn();

		Screen.print("Spiel geladen.");
		System.out.println(file + " loaded.");

		return true;
	}

	/**
	 * Diese Methode Speichert das aktuelle Spielfeld in eine Datei.
	 * 
	 * @param file
	 * @return
	 */
	public boolean save(File file) {

		Properties save = new Properties();

		save.setProperty("Height", Integer.toString(height));
		save.setProperty("Width", Integer.toString(width));

		// Falls der Spieler einen Schluessel hat, wir er gespeichert.
		if (player.getKey() != null) {
			save.setProperty("KeyX",
					Integer.toString(player.getKey().getPos().coor.x));
			save.setProperty("KeyY",
					Integer.toString(player.getKey().getPos().coor.y));
		}

		// Leben und Torches werden gespeichert
		save.setProperty("Lives", Integer.toString(player.getLives()));
		save.setProperty("Torches", Integer.toString(player.getTorches()));

		// Das, was inter dem Spieler liegt, wird gespeichert.
		if (!(player.getUnderlying() instanceof Air))
			save.setProperty("Underlying",
					translateToString(player.getUnderlying()));

		// Das eigentliche Spielfeld wird gespeichert.
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {

				BoardMember mbr = positions[x][y].member;
				if (translateToString(mbr) != null)
					save.setProperty((x + "," + y), translateToString(mbr));

			}

		// Es wird markiert, dass beim naechten Laden keine Leben mehr generiert
		// werden sollen.
		save.setProperty("livesAlreadyGenerated", "true");

		try {
			FileOutputStream outStream = new FileOutputStream(file);
			save.store(outStream, null);
			outStream.close();
			Screen.print("Spiel gespeichert unter " + file.getName() + ".");
			System.out.println("Game saved on location '"
					+ file.getAbsolutePath() + "'");
			return true;
		} catch (IOException ioe) {
			System.out.println("IO Error");
		}
		return false;

	}

	/**
	 * Wandelt einen Code (Siehe Anforderungen) in ein Boardmember um. Falls
	 * generateLives true ist, wird Air mannchmal zu Live.
	 * 
	 * @param str
	 *            der Code
	 * @param generateLives
	 *            ob Leben generiert werden sollen
	 * @return der uebersetzte BoardMember
	 */
	private BoardMember translateToMember(String str, boolean generateLives) {
		if (str == null) {
			Random rand = new Random();
			if (generateLives && (rand.nextFloat() < 0.005))
				return new Live();
			return new Air();
		}
		char chr = str.charAt(0);
		BoardMember mbr;

		switch (chr) {
		case '0':
			mbr = new Wall();
			break;
		case '1':
			mbr = new Entry();
			break;
		case '2':
			mbr = new Exit();
			break;
		case '3':
			mbr = new StaticObst();
			break;
		case '4':
			mbr = new DynamicObst();
			break;
		case '5':
			mbr = new KeyM();
			break;
		case '6':
			mbr = new Live();
			break;
		case '7':
			mbr = new Torch();
			break;
		case 'P':
			mbr = new Player();
			break;

		default:
			mbr = null;
		}

		return mbr;
	}

	/**
	 * Uebersetzt einen Boardmember in den in den Anforderungen gestellten Code.
	 * 
	 * @param mbr
	 *            der zu uebersetzende Member
	 * @return Code
	 */
	private String translateToString(BoardMember mbr) {
		if (mbr instanceof Air)
			return null;
		else if (mbr instanceof Wall)
			return "0";
		else if (mbr instanceof Entry)
			return "1";
		else if (mbr instanceof Exit)
			return "2";
		else if (mbr instanceof StaticObst)
			return "3";
		else if (mbr instanceof DynamicObst)
			return "4";
		else if (mbr instanceof KeyM)
			return "5";
		else if (mbr instanceof Live)
			return "6";
		else if (mbr instanceof Torch)
			return "7";
		else if (mbr instanceof Player)
			return "P";
		else
			return "ERROR";
	}

}
