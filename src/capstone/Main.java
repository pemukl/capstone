package capstone;

import java.awt.Font;
import java.awt.event.WindowAdapter;

import javax.swing.JFrame;

import capstone.menu.MainMenu;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.terminal.TerminalSize;
import com.googlecode.lanterna.terminal.Terminal.ResizeListener;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;
import com.googlecode.lanterna.terminal.swing.TerminalAppearance;

public class Main {

	public static SwingTerminal term;
	private static boolean running = true;
	private static MainMenu mainMenu;
	private static Screen actualScreen;

	public static void main(String[] args) {
		mainMenu = new MainMenu();
		actualScreen = mainMenu;
		initTerm();

		try { // Falls Exceptions geschmissen werden, wird das Terminal trotzdem
				// mit finally geschlossen.

			while (running) {
				Key input = term.readInput();
				
				
				if ((input != null) && (input.getKind() == Key.Kind.Delete))
					kill();
				//Das angezeigte Menue wird durch das naechste ersetzt
				actualScreen = actualScreen.getNextScreen(input);

				// Pause, damit es nicht zu schnell in die naechste Runde geht.
				delay(5); 
			}

		} finally {
			System.out.println("Close terminal.");
			term.exitPrivateMode();
		}

	}

	public static MainMenu getMainMenu() {
		return mainMenu;
	}

	/**
	 * Eine Zeitverzoegerung
	 * @param ms Wartezeit in Millisekunden
	 */
	private static void delay(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * Baut das Terminal und setzt einen ResizeListener auf den aktuellen Screen.
	 */
	private static void initTerm() {
		TerminalAppearance appearance = TerminalAppearance.DEFAULT_APPEARANCE;
		term = TerminalFacade.createSwingTerminal(appearance);
		term.enterPrivateMode();
		JFrame f = term.getJFrame();

		WindowAdapter adapter = new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				kill();
			}
		};

		f.addWindowListener(adapter);

		ResizeListener listener = new ResizeListener() {

			@Override
			public void onResized(TerminalSize newSize) {
				actualScreen.actOnResize();
			}
		};

		term.addResizeListener(listener);

		term.setCursorVisible(false);
	}

	/**
	 * Toetet das gesamte Programm
	 */
	public static void kill() {
		running = false;
	}

}